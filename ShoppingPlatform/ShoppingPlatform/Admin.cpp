#include "Admin.h"

Admin::Admin(int idadm, bool rgt)
{
	idadmin = idadm;
	createRight = rgt;
}

void Admin::setIdAdmin(int idadm) {

	idadmin = idadm;
}

int Admin::getIdAdmin() {

	return idadmin;
}

void Admin::setCreateRight(bool rgt)
{
	createRight = rgt;
}

bool Admin::getCreateRight()
{
	return createRight;
}