#pragma once

#include <QWidget>
#include <QtSql/qtsqlglobal.h>

#include"IScene.h"
#include <ui_DeleteAccountScene.h>

namespace Ui { class DeleteAccountScene; };

class DeleteAccountScene : IScene
{
	Q_OBJECT

public:
	DeleteAccountScene(const std::string& sceneNam);
	~DeleteAccountScene();
	virtual void createScene();
	virtual void clicked();

private:
	QWidget* widget;
};
