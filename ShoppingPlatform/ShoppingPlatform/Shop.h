#pragma once
#include"Seller.h"
class Shop
{

private:
    int idseller;
    int idshop;
    std::string shopName;
    std::string shopAddress;
    std::string phone;
    std::string cui;
    double totalSales;

public:
    Shop(int idslr, int idsh, std::string shName, std::string shAddress, std::string ph, std::string cu, double ttSales);
    void setIdShop(int idsh);
    int getIdShop();
    void setIdSeller(int idsl);
    int getIdSeller();
    void setShopName(std::string shName);
    std::string getShopName();
    void setShopAddress(std::string shAddress);
    std::string getShopAddress();
    void setPhone(std::string phn);
    std::string getPhone();
    void setCui(std::string cu);
    std::string getCui();
    void setTotalSales(double ttSales);
    double getTotalSales();

};

