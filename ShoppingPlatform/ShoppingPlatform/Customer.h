#pragma once
#include "User.h"
class Customer :
	public User
{

private:
	int idcustomer;
	std::string address;
	float totalSpends;
	std::string securityQuestion;
	std::string securityAnswer;

public:
	Customer();
	Customer(int idcx, std::string adr = "Unidentified");
	void setIdCustomer(int idcx);
	int getIdCustomer();
	void setAddress(std::string adr);
	std::string getAddress();
	void setTotalSpends(float spends);
	float getTotalSpends();
	void setSecurityQuestion(std::string que);
	std::string getSecurityQuestion();
	void setSecurityAnswer(std::string que);
	std::string getSecurityAnswer();
};

