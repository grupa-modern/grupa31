
#include "NewOrderScene.h"

NewOrderScene::NewOrderScene(const std::string& sceneNam) :IScene(sceneNam) {

}

NewOrderScene::~NewOrderScene()
{
	delete widget;

}

void NewOrderScene::createScene() {

	auto ui = std::make_unique<NewOrderScene>();
	widget = ui->widget;

	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(searchPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(addPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(viewCartPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(mainViewPB()));

}

void NewOrderScene::searchPB() {
	std::string search = widget->findChild< QPlainTextEdit*>("productSearchTBr")->toPlainText().toStdString();
	std::vector<int> price;
	std::list <std::string> product;
	QStringList stringList;
	QString string;
	QSqlQuery queryString;
	queryString.prepare("SELECT * FROM product WHERE name=search " "VALUES (:search)");
	queryString.bindValue(":search", search.c_str());
	queryString.exec();

	while (queryString.next()) {
		if (queryString.value("stillactive").toBool()) {
			price.push_back(queryString.value("ppu").toInt());
			product.push_back(queryString.value("name").toString().toStdString());
		}
	}
	auto l_front = product.begin();
	for (int i=0; i<product.size(); i++)
	{
		stringList.emplace(i, (std::advance(l_front, i), price.at(i)));
	}
	

	QModelIndex index = widget->findChild<QListView*>("productLV")->currentIndex();
	QString itemIndex = index.data(Qt::DisplayRole).toString();

	widget->findChild<QListView*>("productsLV")->
}

void NewOrderScene::addPB() {
	widget->findChild<QListView*>("productsLV")->currentIndex().data().toString();0
}

void NewOrderScene::viewCartPB() {
	emit sceneChange("Shopping cart");
}

void NewOrderScene::mainViewPB() {
	emit sceneChange("Customer main");
}
