#include "Seller.h"

Seller::Seller(int idslr, std::string spn)
{
	idseller = idslr;
	shopName = spn;
}

void Seller::setIdSeller(int idslr) {

	idseller = idslr;
}

int Seller::getIdSeller() {

	return idseller;
}

void Seller::setShopName(std::string spn)
{
	shopName = spn;
}

std::string Seller::getShopName()
{
	return shopName;
}