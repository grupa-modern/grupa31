#pragma once

#include <QWidget>

#include <QtSql/qtsqlglobal.h>
#include <qmessagebox.h>

#include <ui_CustomerMainScene.h>
#include "IScene.h"
namespace Ui { class CustomerMainScene; };

class CustomerMainScene : IScene
{
	Q_OBJECT
public:
	CustomerMainScene(const std::string& sceneNam);
	~CustomerMainScene();
	virtual void createScene();
	virtual void clicked();

private slots:
	void changeInfoPB();
	void changePwPB();
	void startOrderPB();
	void shoppingCartPB();
	void previousOrdersPB();
	void deleteAccount();

private:
	QWidget* widget;
	std::string findAddress();
	std::string addresss;
};
