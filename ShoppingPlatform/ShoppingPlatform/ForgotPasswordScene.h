#pragma once

#include <QWidget>
#include <QtSql/qtsqlglobal.h>

#include "global.h"
#include "IScene.h"
#include "Customer.h"
#include "LogInScene.h"

#include<ui_ForgotPasswordScene.h>

namespace Ui { class ForgotPasswordScene; };

class ForgotPasswordScene : IScene

{
	Q_OBJECT

public:
	ForgotPasswordScene(const std::string& sceneNam);
	~ForgotPasswordScene();
	virtual void createScene();
	virtual void clicked();
	void cursorPositionChanged();

public slots:
	void createPwPB();
	void emailTB();
	void cancelPB();

private:
	QWidget* widget;
};
