#pragma once

#include <qplaintextedit.h>
#include <regex>

#include "IScene.h"
#include "Customer.h"
#include "ui_ChangeCustomerInfoScene.h"

class ChangeCustomerInfoScene : public IScene
{
	Q_OBJECT

public:
	ChangeCustomerInfoScene(const std::string& sceneNam);
	~ChangeCustomerInfoScene();
	virtual void createScene();
	virtual void clicked();

public slots:
	void changeInfoPB();
	void cancelPB();

private:
	QWidget* widget;
	Customer customer;
	std::string findAddress();
	std::string addresss;
	void addCustomerInfo();
	void addUserInfo();
	void updateUsedUser();
	bool uniqueCheck();
	bool emailCheck();
};
