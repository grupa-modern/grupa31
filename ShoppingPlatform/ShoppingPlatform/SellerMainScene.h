#pragma once

#include <QWidget>
#include<QtSql/qtsqlglobal.h>

#include "IScene.h"
#include"DeleteAccountScene.h"
#include "global.h"

#include <ui_SellerMainScene.h>

namespace Ui { class SellerMainScene; };

class SellerMainScene : IScene
{
	Q_OBJECT

public:
	SellerMainScene(const std::string& sceneNam);
	~SellerMainScene();
	void clicked();
	virtual void createScene();

	private slots:
	/*	void on_changeInfoPB_clicked();
		void on_changePwPB_clicked();
		void on_unshippedPB_clicked();
		void on_shippedPB_clicked();
		void on_addProductPB_clicked();
		void on_deleteProductPB_clicked();
		void on_updateProductPB_clicked();
		void on_getProductPB_clicked();
		void on_deleteAccountPB_clicked();*/


private:
	QWidget* widget;
};
