#pragma once
#include"Shop.h"

class Product
{

private:
	int idproduct;
	int idshop;
	std::string name;
	int quantity;
	float ppu;
	bool stillactive;

public:
	Product(int idpr, int idsh, std::string nu, int qua, float pp, bool stac);
	void setIdProduct(int idpr);
	int getIdProduct();
	void setIdShop(int idsh);
	int getIdShop();
	void setName(std::string nu);
	std::string getName();
	void setQuantity(int qua);
	int getQuantity();
	void setPricePerUnit(float pp);
	float getPricePerUnit();
	void setStillActive(bool stac);
	bool getStillActive();

};

