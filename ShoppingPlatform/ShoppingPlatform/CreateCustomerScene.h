#pragma once


#include <qobject.h>
#include <qplaintextedit.h>
#include <qmessagebox.h>

#include "LogInScene.h" 
#include "IScene.h"
#include "Customer.h"
#include "ui_CreateCustomerScene.h"


namespace Ui { class CreateCustomerScene; };

class CreateCustomerScene : IScene
{

	Q_OBJECT


public:
	CreateCustomerScene(const std::string& sceneNam);
	~CreateCustomerScene();
	void clicked();
	virtual void createScene();


public slots:
	void cancelPB();
	void createAccountPB();


private:
	QWidget* widget;
	bool emailCheck(Customer customer);
	bool uniqueCheck(Customer customer);
	void addUser(Customer customer);
	void addCustomer(Customer customer);


};
