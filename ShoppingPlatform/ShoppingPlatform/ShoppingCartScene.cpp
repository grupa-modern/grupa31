#include "ShoppingCartScene.h"
#include "ui_ShoppingCartScene.h"

ShoppingCartScene::ShoppingCartScene(const std::string& sceneNam) :IScene(sceneNam) {

}


ShoppingCartScene::~ShoppingCartScene()
{
	delete widget;
}

void ShoppingCartScene::createScene() {

	auto ui = std::make_unique<ShoppingCartScene>();
	widget = ui->widget;

	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(deletePB()));

}

void ShoppingCartScene::
