#pragma once

#include <QWidget>

#include"IScene.h"

namespace Ui { class ShoppingCartScene; };

class ShoppingCartScene : IScene
{
	Q_OBJECT

public:
	ShoppingCartScene(const std::string& sceneNam);
	~ShoppingCartScene();
	virtual void createScene();
	virtual void clicked();

private:
	QWidget* widget;
};
