#pragma once

#include <QWidget>
#include <qplaintextedit.h>

#include<list>
#include<vector>

#include "ui_NewOrderScene.h"
#include "IScene.h"

class NewOrderScene : IScene
{

		Q_OBJECT
public:
	NewOrderScene(const std::string& sceneNam);
	~NewOrderScene();
	void clicked();
	virtual void createScene();

public slots:
	void searchPB();
	void addPB();
	void viewCartPB();
	void mainViewPB();

private:
	QWidget* widget;

};
