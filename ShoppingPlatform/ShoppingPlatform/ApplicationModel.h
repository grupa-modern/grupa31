#pragma once
#include "IApplicationModel.h"

#include "LogInScene.h"
#include "CreateCustomerScene.h"
#include "ForgotPasswordScene.h"
#include "AdminMainScene.h"
#include "CustomerMainScene.h"
#include "SellerMainScene.h"
#include "ChangePasswordScene.h"
#include "ChangeCustomerInfoScene.h"
#include "NewOrderScene.h"

#include "ShoppingCartScene.h"
#include "PreviousOrdersScene.h"
#include "DeleteAccountScene.h"

class ApplicationModel:IApplicationModel
{
	ApplicationModel();

	virtual void defineScene();
	virtual void defineFirstScene();

private:
	std::shared_ptr<LogInScene> logInScene;
	std::shared_ptr<CreateCustomerScene>createCustomerScene;
	std::shared_ptr<ForgotPasswordScene> forgotPasswordScene;
	std::shared_ptr<CustomerMainScene> customerMainScene;
	std::shared_ptr<SellerMainScene> sellerMainScene;
	std::shared_ptr<AdminMainScene> adminMainScene;
	std::shared_ptr<ChangePasswordScene> changePasswordScene;
	std::shared_ptr<ChangeCustomerInfoScene> changeCustomerInfoScene;
	std::shared_ptr<NewOrderScene> newOrderScene;
	std::shared_ptr<ShoppingCartScene> shoppingCartScene;
	std::shared_ptr<PreviousOrdersScene> previousOrdersScene;
	std::shared_ptr<DeleteAccountScene> deleteAccountScene;


};

