#include "Shop.h"

Shop::Shop(int idslr, int idsh, std::string shName, std::string shAddress, std::string ph, std::string cu, double ttSales)
{
	idseller = idslr;
	idshop = idsh;
	shopName = shName;
	shopAddress = shAddress;
	phone = ph;
	cui = cu;
	totalSales = ttSales;
}

void Shop::setIdShop(int idsh) {

	idshop = idsh;
}

int Shop::getIdShop() {

	return idshop;
}

void Shop::setIdSeller(int idslr) {

	idseller = idslr;
}

int Shop::getIdSeller() {

	return idseller;
}

void Shop::setShopName(std::string shName) {
	shopName = shName;
}

std::string Shop::getShopName() {
	return shopName;
}

void Shop::setShopAddress(std::string shAddress) {
	shopAddress = shAddress;
}

std::string Shop::getShopAddress() {
	return shopAddress;
}

void Shop::setPhone(std::string ph) {
	phone = ph;
}

std::string Shop::getPhone() {
	return phone;
}

void Shop::setCui(std::string cu) {
	cui = cu;
}

std::string Shop::getCui() {
	return cui;
}

void Shop::setTotalSales(double ttSales) {
	totalSales = ttSales;
}

double Shop::getTotalSales() {
	return totalSales;
}