#pragma once

#include <QWidget>

#include"IScene.h"

namespace Ui { class PreviousOrdersScene; };

class PreviousOrdersScene : IScene
{
	Q_OBJECT

public:
	PreviousOrdersScene(const std::string& sceneNam);
	~PreviousOrdersScene();
	virtual void createScene();
	virtual void clicked();

private:
	QWidget* widget;
};
