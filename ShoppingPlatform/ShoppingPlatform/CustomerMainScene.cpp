#include "CustomerMainScene.h"

CustomerMainScene::CustomerMainScene(const std::string& sceneNam) :IScene(sceneNam) {

}
CustomerMainScene::~CustomerMainScene()
{
	delete widget;
}

void CustomerMainScene::clicked() {
	delete widget;
}

void CustomerMainScene::createScene() {

	auto ui = std::make_unique<CustomerMainScene>();
	widget = ui->widget;

	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(changeInfoPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(changePwPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(startOrderPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(shoppingCartPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(previousOrdersPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(deleteAccount()));

	addresss = findAddress();
	widget->findChild<QLabel*>("nameInfoLB")->setText(userUsed.getName().c_str());
	widget->findChild<QLabel*>("usernameInfoLB")->setText(userUsed.getUsername().c_str());
	widget->findChild<QLabel*>("emailInfoLB")->setText(userUsed.getEmail().c_str());
	widget->findChild<QLabel*>("addressInfoLB")->setText(addresss.c_str());

}

std::string CustomerMainScene::findAddress() {
	QSqlQuery queryString;
	queryString.prepare("SELECT address FROM user WHERE iduser " "VALUES (:iduser)");
	queryString.bindValue(":username", userUsed.getIdUser());
	queryString.exec();
	//addresss.c_str()=queryString.value();

}

void CustomerMainScene::changeInfoPB() {
	previousScene = getSceneName();
	emit sceneChange("Change info");
}

void CustomerMainScene::changePwPB() {
	
	previousScene = getSceneName();
	emit sceneChange("Change password");
}

void CustomerMainScene::startOrderPB() {
	previousScene = getSceneName();
	emit sceneChange("New order");
}

void CustomerMainScene::shoppingCartPB() {
	emit sceneChange("Shopping cart");

}

void CustomerMainScene::previousOrdersPB() {
	emit sceneChange("Previous orders");

}

void CustomerMainScene::deleteAccount() {
	emit sceneChange("Delete account");

}