#include "Customer.h"

Customer::Customer() {

}

Customer::Customer(int idcx, std::string adr)
{
	idcustomer = idcx;
	address = adr;
}

void Customer::setIdCustomer(int idcx) {

	idcustomer = idcx;
}

int Customer::getIdCustomer() {

	return idcustomer;
}

void Customer::setAddress(std::string adr)
{
	address = adr;
}

std::string Customer::getAddress()
{
	return address;
}

void Customer::setTotalSpends(float spends) {

	totalSpends = spends;

}

float Customer::getTotalSpends() {

	return totalSpends;
}

void Customer::setSecurityQuestion(std::string que) {
	securityQuestion = que;
}

std::string Customer::getSecurityQuestion() {
	return securityQuestion;
}

void Customer::setSecurityAnswer(std::string asw) {
	securityAnswer = asw;
}

std::string Customer::getSecurityAnswer() {
	return securityAnswer;
}