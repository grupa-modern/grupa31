#include "ChangeCustomerInfoScene.h"

ChangeCustomerInfoScene::ChangeCustomerInfoScene(const std::string& sceneNam) :IScene(sceneNam) {

}


ChangeCustomerInfoScene::~ChangeCustomerInfoScene()
{
	delete widget;
}

void ChangeCustomerInfoScene::createScene() {
	auto ui = std::make_unique<ChangeCustomerInfoScene>();
	widget = ui->widget;

	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(cancelPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(changeInfoPB()));


	addresss = findAddress();
	widget->findChild<QLabel*>("nameTB")->setText(userUsed.getName().c_str());
	widget->findChild<QLabel*>("usernameTB")->setText(userUsed.getUsername().c_str());
	widget->findChild<QLabel*>("emailTB")->setText(userUsed.getEmail().c_str());
	widget->findChild<QLabel*>("addressTB")->setText(addresss.c_str());

}

void ChangeCustomerInfoScene::clicked() {

	delete widget;
}

void ChangeCustomerInfoScene::cancelPB() {
	emit sceneChange(previousScene);

}

std::string ChangeCustomerInfoScene::findAddress() {
	QSqlQuery queryString;
	queryString.prepare("SELECT address FROM user WHERE iduser " "VALUES (:iduser)");
	queryString.bindValue(":username", userUsed.getIdUser());
	queryString.exec();
	queryString.value(addresss.c_str());

}

void ChangeCustomerInfoScene::changeInfoPB() {
	QMessageBox messageBox;

	customer.setIdUser(userUsed.getIdUser());
	customer.setUsername(widget->findChild<QPlainTextEdit*>("emailTB")->toPlainText().toStdString());
	customer.setEmail(widget->findChild<QPlainTextEdit*>("emailTB")->toPlainText().toStdString());
	customer.setName(widget->findChild<QPlainTextEdit*>("nameTB")->toPlainText().toStdString());
	customer.setAddress(widget->findChild<QPlainTextEdit*>("addressTB")->toPlainText().toStdString());

	if (emailCheck()) {
		if (uniqueCheck()) {
			addCustomerInfo();
			updateUsedUser();
			messageBox.setInformativeText("Information updated!");
			messageBox.exec();
			emit sceneChange(previousScene);
		}
		else {
			messageBox.setInformativeText("Email or username not available!!");
			messageBox.exec();
		}
	}
	else {
		messageBox.setInformativeText("Invalid email address!");
		messageBox.exec();
	}

}

void ChangeCustomerInfoScene::addCustomerInfo() {

	QSqlQuery queryString;

	queryString.prepare("INSERT INTO customer (address) WHERE iduser = iduserr "
		"VALUES  (:address");
	queryString.bindValue(":iduserr", customer.getIdUser());
	queryString.bindValue(":address", customer.getAddress().c_str());
	queryString.exec();
	addUserInfo();
}

void ChangeCustomerInfoScene::addUserInfo() {
	QSqlQuery queryString;

	queryString.prepare("INSERT INTO user (username, email, name) WHERE iduser=iduser "
		"VALUES (:username, :email, :name)");
	queryString.bindValue(":idUser", customer.getIdUser());
	queryString.bindValue(":username", customer.getUsername().c_str());
	queryString.bindValue(":name", customer.getName().c_str());
	queryString.bindValue(":email", customer.getEmail().c_str());
	queryString.exec();

}

bool ChangeCustomerInfoScene::emailCheck() {
	const std::regex pattern
	("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");

	return std::regex_match(userUsed.getEmail(), pattern);
}


bool ChangeCustomerInfoScene::uniqueCheck() {
	QSqlQuery queryString;
	int count = 0;
	bool result = true;
	if (queryString.exec("SELECT * FROM user ")) {
		while (queryString.next()) {
			if (queryString.value("username").toString().toStdString()._Equal(userUsed.getUsername()) && queryString.value("email").toString().toStdString()._Equal(userUsed.getEmail())) {
				count++;
			}
		}
	}
	if(count==1)
		result = false;
	return result;

}

void ChangeCustomerInfoScene::updateUsedUser() {
	userUsed.setUsername(widget->findChild<QPlainTextEdit*>("emailTB")->toPlainText().toStdString());
	userUsed.setEmail(widget->findChild<QPlainTextEdit*>("emailTB")->toPlainText().toStdString());
	userUsed.setName(widget->findChild<QPlainTextEdit*>("nameTB")->toPlainText().toStdString());
}
