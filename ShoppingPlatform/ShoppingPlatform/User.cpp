#include "User.h"
#include <iostream>

User::User(int Iduser,std::string Username, std::string Password, std::string Email, std::string Name, int Type)
{
	iduser = Iduser;
	username = Username;
	password = Password;
	email = Email;
	name = Name;
	type = Type;
}

void User::setIdUser(int idusr) {

	iduser = idusr;
}

int User::getIdUser(){

	return iduser;
}

void User::setUsername(std::string usrn)
{
	username = usrn;
}

std::string User::getUsername()
{
	return username;
}

void User::setEmail(std::string eml)
{
	email = eml;
}

std::string User::getEmail()
{
	return email;
}

void User::setPassword(std::string pwd)
{
	password = pwd;
}

std::string User::getPassword()
{
	return password;
}

void User::setName(std::string nume)
{
	name = nume;
}

std::string User::getName()
{
	return name;
}

void User::setType(int tpe)
{
	type = tpe;
}

int User::getType()
{
	return type;
}