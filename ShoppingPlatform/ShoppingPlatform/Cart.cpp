#include "Cart.h"

Cart::Cart(int idca, int idcu, int idpr, int qua) {
	idcart = idca;
	idcustomer = idcu;
	idproduct = idpr;
	quantity = qua;
}

void Cart::setIdCart(int idca) {
	idcart = idca;
}

int Cart::getIdCart() {
	return idcart;
}

void Cart::setIdCustomer(int idcu) {
	idcustomer = idcu;
}

int Cart::getIdCustomer() {
	return idcustomer;
}

void Cart::setIdProduct(int idpr) {
	idproduct = idpr;
}

int Cart::getIdProduct() {
	return idproduct;
}

void Cart::setQuantity(int qua) {
	quantity = qua;
}

int Cart::getQuantity() {
	return quantity;
}