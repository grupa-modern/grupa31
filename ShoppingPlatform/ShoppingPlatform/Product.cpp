#include "Product.h"

Product::Product(int idpr, int idsh, std::string nu, int qua, float pp, bool stac) {
	idproduct = idpr;
	idshop = idsh;
	name = nu;
	quantity = qua;
	ppu = pp;
	stillactive = stac;
}

void Product::setIdProduct(int idpr) {
	idproduct = idpr;
}
int Product::getIdProduct() {
	return idproduct;
}

void Product::setIdShop(int idsh) {
	idshop = idsh;
}
int Product::getIdShop() {
	return idshop;
}

void Product::setName(std::string nu) {
	name = nu;
}
std::string Product::getName() {
	return name;
}

void Product::setQuantity(int qua) {
	quantity = qua;
}
int Product::getQuantity() {
	return quantity;
}

void Product::setPricePerUnit(float pp) {
	ppu = pp;
}

float Product::getPricePerUnit() {
	return ppu;
}

void Product::setStillActive(bool stac) {
	stillactive = stac;
}

bool Product::getStillActive() {
	return stillactive;
}