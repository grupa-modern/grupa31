#pragma once
#include "User.h"

class Seller :
    public User
{
private:
    int idseller;
    std::string shopName;

public:
    Seller(int idslr, std::string shopName);
    void setIdSeller(int idslr);
    int getIdSeller();
    void setShopName(std::string spn);
    std::string getShopName();
};
