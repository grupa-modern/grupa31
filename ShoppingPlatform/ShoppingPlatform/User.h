#pragma once

#include <string>
#include <vector>

class User
{

protected:
	int iduser;
	std::string username;
	std::string password;
	std::string email;
	std::string name;
	int type;

public:
	User(int Iduser = 1, std::string Username = "Unidentified", std::string Password = "Unidentified", std::string Email = "Unidentified", std::string Name = "Unidentified", int type=1);
	void setIdUser(int idusr);
	int getIdUser();
	void setUsername(std::string usrn);
	std::string getUsername();
	void setEmail(std::string eml);
	std::string getEmail();
	void setPassword(std::string pwd);
	std::string getPassword();
	void setName(std::string nume);
	std::string getName();
	void setType(int typ);
	int getType();
};
