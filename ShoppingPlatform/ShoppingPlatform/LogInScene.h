#pragma once

#include <regex>
//#include <soci.h>

#include <qobject.h>
#include <qmessagebox.h>
#include <qplaintextedit.h>

#include<QtSql/qsqldatabase.h>
#include<QtSql/qsqldriver.h>
#include<QtSql/qsqlerror.h>
#include<QtSql/qsqlquery.h>

#include "User.h"
#include "IScene.h"

namespace Ui { class LogInScene; };

class LogInScene : IScene
{
    Q_OBJECT

public:
    LogInScene(const std::string& sceneNam);
    ~LogInScene();
    virtual void clicked();
    virtual void createScene();
    //void connnectMySql(QSqlDatabase db);
   // void disconnectMySql(QSqlDatabase db);

public slots:
    void createAccountPB();
    void forgotPwdPB();
    void logInPB();

private:
    QWidget *widget;
    int fieldsCheck(User user);
    bool checkEmail(const std::string& email);
    //QSqlDatabase db;
    
};

