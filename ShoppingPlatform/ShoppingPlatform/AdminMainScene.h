#pragma once

#include <QWidget>
#include <QtSql/qtsqlglobal.h>

#include "global.h"
#include "IScene.h"
//#include "qthread-int.h"

namespace Ui { class AdminMainScene; };

class AdminMainScene : IScene

{
	Q_OBJECT

public:
	AdminMainScene(const std::string& sceneNam);
	~AdminMainScene();
	virtual void createScene();
	virtual void clicked();

private:
	//Ui::AdminMainScene *ui;
	QWidget* widget;
};
