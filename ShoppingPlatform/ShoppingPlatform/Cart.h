#pragma once
class Cart
{

private:
	int idcart;
	int idcustomer;
	int idproduct;
	int quantity;

private:
	Cart(int idca, int idcu, int idpr, int qua);
	void setIdCart(int idca);
	int getIdCart();
	void setIdCustomer(int idcu);
	int getIdCustomer();
	void setIdProduct(int idpr);
	int getIdProduct();
	void setQuantity(int qua);
	int getQuantity();

};

