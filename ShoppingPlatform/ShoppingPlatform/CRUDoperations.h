#pragma once

#include "User.h"
#include "Customer.h"
#include "Seller.h"
#include "Admin.h"
#include <list>
#include "Client.h"
#include <soci.h>
#include <soci-mysql.h>



//to be added to the client class, but not sure exactly regarding the architecture yet

class CRUDoperations
{

public:
	template <typename T> void createMethod(T createArgument);
	template <typename T> std::list<T> readMethod();
	template <typename T> void updateMethod(T updateArgument);
	template <typename T> void deleteMethod(T deleteArgument);
	std::string queryString;

	std::string tableName; 
	soci::session& session;
	 
private:
	void createUser(User createArgument);
	void createCustomer(Customer createArgument);
	void createSeller(Seller createArgument);
	void createAdmin(Admin createArgument);

	std::list<User> readUser();
	std::list<Customer> readCustomer();
	std::list<Seller> readSeller();
	std::list<Admin> readAdmin();

	void updateUser(User updateArgument);
	void updateCustomer(Customer updateCustomer);
	void updateSeller(Seller updateArgument);
	void updateAdmin(Admin updateArgument);

	void deleteUser(User deleteArgumtn);
	void deleteCustomer(Customer deleteArgumtn);
	void deleteSeller(Seller deleteArgumtn);
	void deleteAdmin(Admin deleteArgumtn);

};

