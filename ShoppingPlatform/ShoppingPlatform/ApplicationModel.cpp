#include "ApplicationModel.h"

const std::string logInSceneName = std::string("Login scene");
const std::string createAccountSceneName = std::string("Create account");
const std::string forgotPasswordSceneName = std::string("Forgot password");
const std::string customerMainSceneName = std::string("Customer main");
const std::string sellerMainSceneName = std::string("Seller main");
const std::string adminMainSceneName = std::string("Admin main");
const std::string changePasswordSceneName = std::string("Change password");
const std::string changeCustomerInfoSceneName = std::string("Change info");
const std::string newOrderSceneName = std::string("New order");
const std::string shoppingCartSceneName = std::string("Shopping cart");
const std::string previousOrdersSceneName = std::string("Previous orders");
const std::string deleteAccountSceneName = std::string("Delete account");

ApplicationModel::ApplicationModel():IApplicationModel() {

}

void ApplicationModel::defineScene() {

	logInScene = std::make_shared<LogInScene>(logInSceneName);
	createCustomerScene = std::make_shared<CreateCustomerScene>(createAccountSceneName);
	forgotPasswordScene = std::make_shared<ForgotPasswordScene>(forgotPasswordSceneName);
	customerMainScene = std::make_shared<CustomerMainScene>(customerMainSceneName);
	sellerMainScene = std::make_shared<SellerMainScene>(sellerMainSceneName);
	adminMainScene = std::make_shared<AdminMainScene>(adminMainSceneName);
	changePasswordScene = std::make_shared <ChangePasswordScene>(changePasswordSceneName);
	changeCustomerInfoScene = std::make_shared <ChangeCustomerInfoScene>(changeCustomerInfoSceneName);
	newOrderScene = std::make_shared<NewOrderScene>(newOrderSceneName);
	shoppingCartScene = std::make_shared<ShoppingCartScene>(shoppingCartSceneName);
	previousOrdersScene = std::make_shared<PreviousOrdersScene>(previousOrdersSceneName);
	deleteAccountScene = std::make_shared<DeleteAccountScene>(deleteAccountSceneName);

	scenes.emplace(logInSceneName, logInScene.get());
	scenes.emplace(createAccountSceneName, createCustomerScene.get());
	scenes.emplace(forgotPasswordSceneName, forgotPasswordScene.get());
	scenes.emplace(customerMainSceneName, customerMainScene.get());
	scenes.emplace(sellerMainSceneName, sellerMainScene.get());
	scenes.emplace(adminMainSceneName, adminMainScene.get());
	scenes.emplace(changePasswordSceneName, changePasswordScene.get());
	scenes.emplace(changeCustomerInfoSceneName, changeCustomerInfoScene.get());
	scenes.emplace(newOrderSceneName, newOrderScene.get());
	scenes.emplace(shoppingCartSceneName, shoppingCartScene.get());
	scenes.emplace(previousOrdersSceneName, previousOrdersScene.get());
	scenes.emplace(deleteAccountSceneName, deleteAccountScene.get());

}

void ApplicationModel::defineFirstScene()
{
	firstScene=logInSceneName;
}

