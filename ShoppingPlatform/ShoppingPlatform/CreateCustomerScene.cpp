#include "CreateCustomerScene.h"
#include "ui_CreateCustomerScene.h"


CreateCustomerScene::CreateCustomerScene(const std::string& sceneNam) :IScene(sceneNam) {

}

CreateCustomerScene::~CreateCustomerScene()
{
	delete widget;
}

void CreateCustomerScene::createScene()
{
	auto ui = std::make_unique<CreateCustomerScene>();
	widget = ui->widget;

	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(cancelPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(createAccountPB()));

   /* db = QSqlDatabase::addDatabase("QMYSQL");
	db.setPort(3306);
	db.setUserName("root");
	db.setPassword("123456");
	db.setDatabaseName("shopping");
	db.open();*/
}

void CreateCustomerScene::clicked()
{
	delete widget;
			//if (!password._Equal(againPassword))
		//	qDebug() << "Password don't match. Try again";
		//else {
		//	if (emailCheck(email)) {
		//		if (uniqueCheck(username, email)) {
		//			addUser(username, email, name, address, password);
		//		}
		//		else
		//			//qDebug() << "The username or email alredy associated to an account!";
		//			username = "erroare";
		//	}
		//	else {
		//		//qDebug() << "Email invalid!";
		//	}
		//}
	
}

void CreateCustomerScene::createAccountPB() {
	Customer &customer= Customer();
	int idUser = rand(), idCustomer = rand();
	int type = 1;
	std::string againPassword;
	bool emailValid, userExists = true;
	customer.setIdUser(idUser);
	customer.setIdCustomer(idCustomer);
	customer.setUsername(widget->findChild<QPlainTextEdit*>("usernameTB")->toPlainText().toStdString());
	customer.setEmail(widget->findChild<QPlainTextEdit*>("emailTB")->toPlainText().toStdString());
	customer.setName(widget->findChild<QPlainTextEdit*>("nameTB")->toPlainText().toStdString());
	customer.setAddress(widget->findChild<QPlainTextEdit*>("addressTB")->toPlainText().toStdString());
	customer.setPassword(widget->findChild<QPlainTextEdit*>("passwordTB")->toPlainText().toStdString());
	customer.setType(type);
	customer.setTotalSpends(0);
	againPassword = widget->findChild<QPlainTextEdit*>("retypePwdTB")->toPlainText().toStdString();

	if (!customer.getPassword()._Equal(againPassword)) {
		qDebug() << "passwords do not match";

	}else if (emailCheck(customer)) {
		if (!userExists)
			addUser(customer);
		else
			qDebug() << "Username or passwords exists";
	}
	else
		qDebug() << "Invalid email";		
	
}

void CreateCustomerScene::cancelPB() {

	delete widget;
	emit sceneChange("Login scene");

}

bool CreateCustomerScene::emailCheck(Customer customer) {
	const std::regex pattern
	("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");

	return std::regex_match(customer.getEmail(), pattern);
}

bool CreateCustomerScene::uniqueCheck(Customer customer) {
	QSqlQuery queryString;
	bool result = true;
	if (queryString.exec("SELECT * FROM user ")) {
		while (queryString.next()) {
			if (queryString.value("username").toString().toStdString()._Equal(customer.getUsername()) || queryString.value("email").toString().toStdString()._Equal(customer.getEmail())) {
				result = false;
			}
		}
	}
	return result;

}

void CreateCustomerScene::addUser(Customer customer) {
	std::string username = customer.getUsername(),
		name = customer.getName(), 
		password = customer.getPassword();
	int  idUser=customer.getIdUser(), 
		type = customer.getType();
	QSqlQuery queryString;

	queryString.prepare("INSERT INTO user (iduser, username, email, name, password, type) "
		"VALUES (:idUser, :username, :email, :name, :password,:type)");
	queryString.bindValue(":idUser", idUser);
	queryString.bindValue(":username", username.c_str());
	queryString.bindValue(":name", name.c_str());
	queryString.bindValue(":password", password.c_str());
	queryString.bindValue(":type", type);
	queryString.exec();
	addCustomer(customer);
}

void CreateCustomerScene::addCustomer(Customer customer) {
	std::string securityquestion = "University name?",
		securityanswer = "Transilvania", 
		address = customer.getAddress();
	int  idUser = customer.getIdUser(), 
		idCustomer = customer.getIdCustomer();
	float totalSpends = customer.getTotalSpends();
	QSqlQuery queryString;
	QMessageBox messageBox;

	queryString.prepare("INSERT INTO customer (idcustomer, iduser, address, totalspends, securityquestion, securityanswer) "
		"VALUES (:idCustomer, :idUser, :address,:totalWSpends, :securityquestion, :securityanswer)");
	queryString.bindValue(":idcustomer", idCustomer);
	queryString.bindValue(":iduser", idUser);
	queryString.bindValue(":address", address.c_str());
	queryString.bindValue(":totalspends", totalSpends);
	queryString.bindValue(":securityquestion", securityquestion.c_str());
	queryString.bindValue(":securityanswer", securityanswer.c_str());
	queryString.exec();
	messageBox.setInformativeText("User added!");
	messageBox.exec();
	//db.close();
	emit sceneChange("Login scene");
	
	delete widget;
}