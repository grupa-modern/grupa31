


#include "ChangeScene.h"

ChangeScene::ChangeScene(std::map< std::string, IScene*> collection, std::unique_ptr<QMainWindow> mainn,  std::string first) :
sceneCollection(collection),
mainScene(std::move(mainn)),
firstScene(first)
{
	assert(sceneCollection.size() != 0);
		for ( auto& scene : sceneCollection) {
			QObject::connect(scene.second, SIGNAL(sceneChange( std::string&)), this, SLOT(setCurrentcene( std::string&)));
		}
		getCurrentScene(firstScene);

}

void ChangeScene::setCurrentScene(std::string& sceneName) {

	getCurrentScene(sceneName);

}

void ChangeScene::getCurrentScene( std::string& sceneName) {

	IScene* current = nullptr;

	if (currentScene != sceneName) {
		const auto& it = sceneCollection.find(currentScene);
		current = it->second;
	}
	const auto& nextScene = sceneCollection.find(sceneName)->second;

	currentScene = sceneName;
	
}

