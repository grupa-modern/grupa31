#include "ChangePasswordScene.h"
#include "ui_ChangePasswordScene.h"

ChangePasswordScene::ChangePasswordScene(const std::string& sceneNam) :IScene(sceneNam) {

}

ChangePasswordScene::~ChangePasswordScene()
{
	delete widget;
}

void ChangePasswordScene::createScene() {
	auto ui = std::make_unique<ChangePasswordScene>();
	widget = ui->widget;

	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(changePwdPB()));
}

void ChangePasswordScene::clicked() {
	delete widget;
}


void ChangePasswordScene::changePwdPB() {

	std::string testPwd, currentPwd = widget->findChild<QPlainTextEdit*>("currentPwdTB")->toPlainText().toStdString(),
		newPwd = widget->findChild<QPlainTextEdit*>("newPwdTB")->toPlainText().toStdString(),
		retypeNewPwd = widget->findChild<QPlainTextEdit*>("retypePwdTB")->toPlainText().toStdString();
	QSqlQuery queryString;
	QMessageBox messageBox;

	queryString.prepare("SELECT password FROM customer WHERE username " "VALUES (:username)");
	queryString.bindValue(":username", userUsed.getUsername().c_str());
	testPwd = queryString.exec();
	if (testPwd._Equal(currentPwd)) {
		if (newPwd._Equal(retypeNewPwd)) {
			queryString.prepare("INSERT INTO user (iduser, username, email, name, password, type) WHERE username "
				"VALUES (:username, :password)");
			queryString.bindValue(":username", userUsed.getUsername().c_str());
			queryString.bindValue(":password", newPwd.c_str());
			userUsed.setPassword(newPwd);
			messageBox.setInformativeText("Password updated!");
			messageBox.exec();

		}
		else {
			messageBox.setInformativeText("Password don't match!");
			messageBox.exec();
		}

	}
	else {
		messageBox.setInformativeText("This is not your current password! Retry or select forgot password.");
		messageBox.exec();
	}

	emit sceneChange(previousScene);

}
