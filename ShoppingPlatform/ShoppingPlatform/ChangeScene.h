#pragma once

#include<qobject.h>

#include "IScene.h"
#include "User.h"

class ChangeScene: QObject
{
	ChangeScene(std::map< std::string, IScene*> collection, std::unique_ptr<QMainWindow> mainn,  std::string first);

private:
	std::map<std::string, IScene*> sceneCollection;
	std::unique_ptr<QMainWindow> mainScene;
	std::string firstScene;
	void setCurrentScene( std::string& sceneName);
    std::string currentScene;

private slots:
	void getCurrentScene( std::string& sceneName);
};
