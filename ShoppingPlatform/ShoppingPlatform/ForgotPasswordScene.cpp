#include "ForgotPasswordScene.h"
#include "ui_ForgotPasswordScene.h"

ForgotPasswordScene::ForgotPasswordScene(const std::string& sceneNam) :IScene(sceneNam) {

}

ForgotPasswordScene::~ForgotPasswordScene()
{
	delete widget;
}

void ForgotPasswordScene::createScene() {

	auto ui = std::make_unique<ForgotPasswordScene>();
	widget = ui->widget;

	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(cancelPB()));
	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(createPwPB()));
	QObject::connect(ui.get(), SIGNAL(cursorPositionChanged()), this, SLOT(cursorPositionChanged()));

}

void ForgotPasswordScene::cursorPositionChanged() {

	Customer customer;
	std::string securityQuestion, securityAnswer;
	customer.setUsername(widget->findChild<QPlainTextEdit*>("usernameTB")->toPlainText().toStdString());
	QSqlQuery queryString;
	queryString.prepare("SELECT securityquestion FROM customer WHERE username " "VALUES (:username)");
	queryString.bindValue(":username", customer.getUsername().c_str());queryString.exec();
	securityQuestion = queryString.exec();
	widget->findChild<QLabel*>("securityQuestionLB")->setText(securityQuestion.c_str());

}

void ForgotPasswordScene::cancelPB() {

	emit sceneChange("Login scene");
	delete widget;
}

void ForgotPasswordScene::createPwPB() {

	std::string usernameF = widget->findChild<QPlainTextEdit*>("usernameTB")->toPlainText().toStdString(),
		passwordF = widget->findChild<QPlainTextEdit*>("createPwTB")->toPlainText().toStdString();
	QSqlQuery queryString;
	QMessageBox messageBox;

	queryString.prepare("INSERT INTO user password where username =usernameF"
		"VALUES (:passwordF, :usernameF)");
	queryString.bindValue(":usernameF", usernameF.c_str());
	queryString.bindValue(":password", passwordF.c_str());
	queryString.exec();
	messageBox.setInformativeText("Password changed!");
	messageBox.exec();
	//db.close();
	emit sceneChange("Login scene");
	delete widget;

}