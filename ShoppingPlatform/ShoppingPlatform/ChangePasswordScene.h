#pragma once

#include <QWidget>
#include <QtSql/qtsqlglobal.h>

#include "global.h"
#include "IScene.h"

namespace Ui { class ChangePasswordScene; };

class ChangePasswordScene : IScene
{
	Q_OBJECT

public:
	ChangePasswordScene(const std::string& sceneNam);
	~ChangePasswordScene();
	virtual void clicked();
	virtual void createScene();

public slots:
	void changePwdPB();



private:
	QWidget* widget;
};
