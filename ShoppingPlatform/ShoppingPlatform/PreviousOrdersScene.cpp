#include "PreviousOrdersScene.h"
#include "ui_PreviousOrdersScene.h"

PreviousOrdersScene::PreviousOrdersScene(const std::string& sceneNam) :IScene(sceneNam) {

}


PreviousOrdersScene::~PreviousOrdersScene()
{
	delete widget;
}

void PreviousOrdersScene::createScene() {

	auto ui = std::make_unique<PreviousOrdersScene>();
	widget = ui->widget;

	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(deletePB()));

}
