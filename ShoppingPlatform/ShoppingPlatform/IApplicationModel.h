#pragma once
#include "IScene.h"
#include <map>


class IApplicationModel {
public:
	virtual void defineScene() = 0;
	virtual void defineFirstScene() = 0;

	std::map<const std::string, IScene*> getScenes() const
	{
		return scenes;
	}

	virtual ~IApplicationModel()
	{
		;
	}

protected:
	std::string firstScene;
	std::map<const std::string, IScene*> scenes;
};
