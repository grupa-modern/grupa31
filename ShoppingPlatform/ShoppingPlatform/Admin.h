#pragma once
#include "User.h"
class Admin :
    public User
{
private:
    int idadmin;
    bool createRight;

public:
    Admin(int idadm, bool rgt);
    void setIdAdmin(int idadm);
    int getIdAdmin();
    void setCreateRight(bool rgt);
    bool getCreateRight();
};
