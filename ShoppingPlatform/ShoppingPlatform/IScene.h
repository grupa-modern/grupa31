#pragma once
#include "User.h"
#include<QtSql/qsqldatabase.h>
#include<QtSql/qsqldriver.h>
#include<QtSql/qsqlerror.h>
#include<QtSql/qsqlquery.h>
#include <qmessagebox.h>
#include <QtWidgets/qmainwindow.h>

class IScene: public QObject {

	Q_OBJECT

public:

	User userUsed;
	std::string previousScene;
virtual void createScene() = 0;
	virtual void clicked() = 0;

	  explicit IScene(const std::string& sceneNam) : sceneName(sceneNam)
		{
			;
		}

	  virtual std::unique_ptr<QMainWindow>& getWindow()
	  {
		  return mainWindow;
	  }

	virtual std::string getSceneName() {

		return sceneName;

	}

	virtual ~IScene()
	{
		;
	}


private:
	const std::string sceneName;

protected:
	std::unique_ptr<QMainWindow> mainWindow;

signals:
	void sceneChange(const std::string& sceneNam);

};