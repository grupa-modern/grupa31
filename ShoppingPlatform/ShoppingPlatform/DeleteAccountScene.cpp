#include "DeleteAccountScene.h"

DeleteAccountScene::DeleteAccountScene(const std::string& sceneNam) :IScene(sceneNam) {

}


DeleteAccountScene::~DeleteAccountScene()
{
	delete widget;
}

void DeleteAccountScene::createScene() {

	auto ui = std::make_unique<DeleteAccountScene>();
	widget = ui->widget;

	QObject::connect(ui.get(), SIGNAL(clicked()), this, SLOT(deletePB()));

}