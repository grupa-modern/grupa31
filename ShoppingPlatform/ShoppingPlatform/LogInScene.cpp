#include "LogInScene.h"
#include "ui_LogInScene.h"

LogInScene::LogInScene(const std::string& sceneNam) :IScene(sceneNam) {

}

void LogInScene::createScene()
{
    auto ui = std::make_unique<Ui_LogInSceneClass>();
    
    ui->setupUi(mainWindow.get());

    QObject::connect(ui->createAccountPB, SIGNAL(clicked()), this, SLOT(createAccountPB()));
    QObject::connect(ui->forgotPwdPB, SIGNAL(clicked()), this, SLOT(forgotPwdPB()));
    QObject::connect(ui->logInPB, SIGNAL(clicked()), this, SLOT(logInPB()));

}

LogInScene::~LogInScene()
{
    delete widget;
}

void LogInScene::clicked()
{
    delete widget;
   
}

void LogInScene::createAccountPB() {
    emit sceneChange("Create account");
}

void LogInScene::forgotPwdPB() {

    emit sceneChange("Forgot password");
}

void LogInScene::logInPB() {
    User user;
    QMessageBox messageBox;
    user.setUsername(widget->findChild<QPlainTextEdit*>("usernameTB")->toPlainText().toStdString());
    user.setPassword(widget->findChild<QPlainTextEdit*>("passwordTB")->toPlainText().toStdString());
    int result = fieldsCheck(user);

    switch (result) {
    case 1:
        emit sceneChange("Customer main");
        userUsed = user;
        delete widget;
        break;
    case 2: 
        emit sceneChange("Seller main");
        userUsed = user;
        delete widget;
        break;
    case 3:
        emit sceneChange("Admin main");
        userUsed = user;
        delete widget;
        break;
    default:
        messageBox.setText("Username and/or password invalid!");
        messageBox.exec();
        break;
    }    
}

int LogInScene::fieldsCheck(User user) {
    
    int result = 0;
    QSqlQuery queryString;
    queryString.prepare("SELECT type FROM user WHERE username " "VALUES (:username)");
    queryString.bindValue(":username", user
        .getUsername().c_str());
    result = queryString.exec();

    return result;   
}

bool emailCheck(const std::string& email)
{

    const std::regex pattern(
        "(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
    return regex_match(email, pattern);
}









